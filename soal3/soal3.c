#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>

char *lname;

struct dirent *dStru;

void forking(char cmd[], char *arg[]) {
    pid_t pid = fork();
    int s;
    if(pid == 0){
        execv(cmd, arg);
    }
    else {
        ((wait(&s)) > 0);
    }
}

void forkMove(char fileName[], char pathDestination[]) {
    pid_t pid = fork();
    int s;
    if(pid == 0) {
        execlp("mv", "mv", fileName, pathDestination, NULL);
    }
    else {
        ((wait(&s)) > 0);
    }
}

void forkRemove(char fileName[]) {
    pid_t pid = fork();
    int s;
    if(pid == 0) {
        execlp("rm", "rm", fileName, NULL);
    }
    else {
        ((wait(&s)) > 0);
    }
}

void CreateDirAndUnzip() {
    char *modulArg[] = {"mkdir", "modul2", NULL};
    forking("/bin/mkdir", modulArg);

    char *animalArg[] = {"mkdir", "modul2/animal", NULL};
    forking("/bin/mkdir", animalArg);

    char *daratArg[] = {"mkdir", "modul2/darat", NULL};
    forking("/bin/mkdir", daratArg);

    sleep(3);
    
    char *airArg[] = {"mkdir", "modul2/air", NULL};
    forking("/bin/mkdir", airArg);

    char *unzipArg[] = {"unzip", "animal.zip", "-d", "./modul2", NULL};
    forking("/usr/bin/unzip", unzipArg);
}

void Checker() {

    DIR *unzLoc;

    unzLoc = opendir("./modul2/animal");

    if(unzLoc != NULL) {
        while((dStru = readdir(unzLoc))) {
            if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0 && strstr(dStru->d_name, "darat")) {
                char started[128] = "modul2/animal/";
                strcat(started, dStru->d_name); 
                forkMove(started, "modul2/darat");
            } else if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0 && strstr(dStru->d_name, "air")) {
                char started[128] = "modul2/animal/";
                strcat(started, dStru->d_name); 
                forkMove(started, "modul2/air");
            } else {
                if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0) {
                    char started[128] = "modul2/animal/";
                    strcat(started, dStru->d_name); 
                    forkRemove(started);
                }
                
            }
        }
        (void) closedir (unzLoc);
    } else  {
        perror ("Check again the directory or path!");
    }
}

void CheckerDarat() {
    DIR *daratP;

    daratP = opendir("modul2/darat");
    if(daratP != NULL) {
        while((dStru = readdir(daratP))) {
            if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0 && strstr(dStru->d_name, "bird")) {
                char started[128] = "modul2/darat/";
                strcat(started, dStru->d_name); 
                forkRemove(started);
            }
        }
        (void) closedir (daratP);
    } else  {
        perror ("Check again the directory or path!");
    }
}

void CheckerAir() {
    DIR *airP;

    airP = opendir("modul2/air");
    FILE *listAir = fopen("./modul2/air/list.txt", "w");

    struct stat stats;
    int i;

    if(airP != NULL) {
         while((dStru = readdir(airP))) {
            if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0 && strstr(dStru->d_name, "jpg")) {
                char started[128] = "modul2/air/";
                strcat(started, dStru->d_name);
                i = stat(started, &stats);
                char r = '_', w = '_', x = '_';
                if(stats.st_mode & S_IRUSR) r = 'r';
                if(stats.st_mode & S_IWUSR) w = 'w';
                if(stats.st_mode & S_IXUSR) x = 'x';
                fprintf(listAir, "%s_%c%c%c_%s\n", lname, r, w, x, dStru->d_name);
            }
        }
        (void) closedir (airP);
    } else  {
        perror ("Check again the directory or path!");
    }

    fclose(listAir);
}


int main() {

    lname = (char *)malloc(10*sizeof(char));
    lname = getlogin();

    CreateDirAndUnzip();
    Checker();
    CheckerDarat();

    return 0;
}
