#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

char *uname;
char path[200];
char temp[20];

void bash(char *cmd, char *argv[]){
	int status;
    pid_t child = fork();

    if(child == 0){
        execv(cmd, argv);
    } else while(wait(&status) > 0);
}
	
void unzip(){
	char *argv[ ] = {"unzip", "drakor.zip", "*.png", "-d", "/home/rosfandy/shift2/drakor", NULL};
	execv("/bin/unzip", argv);
}

void mkrepo(char *uname){
	snprintf(path, sizeof path, "/home/%s/shift2/drakor", uname);
	char *argv[ ] = {"mkdir", "-p", path, NULL};	
	execv("/usr/bin/mkdir", argv);
}

void mkfolder(char *str, char *uname){
	snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s", uname, str);
	char *argv[] = {"mkdir", "-p", path, NULL};
	char *cmd = "/usr/bin/mkdir";
	bash(cmd, argv);
}

void copy(char genrefrom[50], char genreto[50], char filename[1000]){
		char pathto[200];
		snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/%s", uname, genreto, filename);
		snprintf(pathto, sizeof pathto, "/home/%s/shift2/drakor/%s/", uname, genrefrom);
		char *argv[] = {"cp", path, pathto, NULL};
		char *cmd = "/usr/bin/cp";
		bash(cmd, argv);
}
void ganti(char judul[50], char genre[200], char filename[1000]){
	char pathto[200];
	snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/%s", uname, genre, filename);
	snprintf(pathto, sizeof pathto, "/home/%s/shift2/drakor/%s/%s.png", uname, genre, judul);
	char *argv[] = {"mv", path, pathto, NULL};
	char *cmd = {"/usr/bin/mv"};
	bash(cmd,argv);
}

void data(char judul[100], char tahun[100], char kategori[100], char genre[100]){
	snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/data.txt", uname, genre);
	FILE *data;
	data = fopen(path, "a");

	

	if (strcmp(temp, kategori) != 0){
		fprintf(data, "kategori : %s\n\n", kategori);
	}
	fprintf(data, "nama : %s\n", judul);
	fprintf(data, "rilis : tahun %s\n\n", tahun);
	
	fclose(data);
	strcpy(temp,kategori);
}

void chname(){
	DIR *repo;
	struct dirent *dir;
	char genre[7][10]={"action","comedy","fantasy","horror","romance","school","thriller"};

	for (int i = 0; i<7; i++){
		snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/", uname, genre[i]);
		repo = opendir(path);
		if (repo){
			while ((dir = readdir(repo)) != NULL){
			if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) continue;
				// printf("%s\wn",path);
				char filename[1000];
				char file[1000];
				char *result;
				snprintf(filename, sizeof filename, "%s", dir->d_name);
				strcpy(file, filename);
				// printf("%s\n", filename);

				result = strstr(file,"_");
				
				if(!result){
					char judul[50];				
					char tahun[50];				
					char kategori[50];
					printf("%s\n", filename);
					char *token = strtok(file,";");
					snprintf(judul, sizeof judul, "%s", token);
					// printf("%s\n", judul);
					token = strtok(NULL, ";");
					
					snprintf(tahun, sizeof judul, "%s", token);
					// printf("%s\n",tahun);
					token = strtok(NULL, ";");
			
					snprintf(kategori, sizeof judul, "%s", token);
					char nama[100];
					snprintf(nama, sizeof judul, "%s", kategori);
					strtok(nama,".");
					// printf("%s\n",nama);
					token = strtok(NULL, ";");
					
					ganti(judul,genre[i],filename);
					data(judul,tahun,nama,genre[i]);
				} else {
					// printf("%s\n",filename);
					char str[1000];
					snprintf(str, sizeof str, "%s", filename);
					char *split = strtok(file,"_");
					while (split!=NULL){
						// printf("%s\n",split);
						char *flag;
						char ext[100];
						snprintf(ext, sizeof ext, "%s.png", genre[i]);
						flag = strstr(split, ext);
						if (flag){
							char judull[50];				
							char tahunn[50];				
							char kategorii[50];

							char *tokenn = strtok(split,";");
							snprintf(judull, sizeof judull, "%s", tokenn);
							tokenn = strtok(NULL, ";");

							snprintf(tahunn, sizeof tahunn, "%s", tokenn);
							tokenn = strtok(NULL, ";");
					
							snprintf(kategorii, sizeof kategorii, "%s", tokenn);
							tokenn = strtok(NULL, ";");

							ganti(judull,genre[i],filename);
							data(judull,tahunn,kategorii,genre[i]);
							
						} else {
							char judull[50];				
							char tahunn[50];				
							char kategorii[50];

							char *tokenn = strtok(split,";");
							snprintf(judull, sizeof judull, "%s", tokenn);
							tokenn = strtok(NULL, ";");

							snprintf(tahunn, sizeof tahunn, "%s", tokenn);
							tokenn = strtok(NULL, ";");
					
							snprintf(kategorii, sizeof kategorii, "%s", tokenn);
							tokenn = strtok(NULL, ";");							

							ganti(judull,genre[i],filename);
							data(judull,tahunn,kategorii,genre[i]);
						}
						split=strtok(NULL,"_");
					}			
				}

			}
		
		}
	}
	
}
void mkdata(){
	DIR *repo;
	struct dirent *dir;
	char genre[7][10]={"action","comedy","fantasy","horror","romance","school","thriller"};
	char data[5][50];
	char d_split[1][50];
	for (int i = 0; i < 7; i++){
	
	snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/", uname, genre[i]);
	repo = opendir(path);
	if (repo)
	{
		while ((dir = readdir(repo)) != NULL)
		{
			char *result;
				char filename[1000];
			    snprintf(filename, sizeof filename, "%s", dir->d_name);
				result = strstr(filename,"_");
				if (result) {
			    	char file[1000];
			    	strcpy(file, filename);
					
					char *split = strtok(file,"_");
					int j = 0;
					while(split !=NULL){
						snprintf(d_split[j], sizeof d_split[j], "%s", split);
						j++;
						split=strtok(NULL,"_");
					}
			    	int k = 0;
						char judul[j][50];
						char tahun[j][50];
						char genre[j][50];
						char kat[50];
			    		for (int j=0; j<2; j++){
							char *token = strtok(d_split[j],";");

							snprintf(judul[j], sizeof judul[j], "%s", token);
							token = strtok(NULL,";");
							// printf("%s\n",judul[j]);

							snprintf(tahun[j], sizeof tahun[j], "%s", token);
							token = strtok(NULL,";");
							// printf("%s\n",tahun[j]);
									    		
							snprintf(genre[j], sizeof genre[j], "%s", token);
							// printf("%s\n",genre[j]);
							token = strtok(NULL,";");
							char *kategori=strtok(genre[1],".");
							
							int i = 0;
							snprintf(kat, sizeof kat, "%s", kategori);
							// printf("%s\n",kat);
							i++;
							kategori = strtok(NULL,".");

			    		}
							copy(genre[0],kat,filename);

					
				}
				
	 	}
	    closedir(repo);
	 }
	 }

	chname();	 
}


void moveFile(char *filename, char *str){
	char pathto[100];
	mkfolder(str, uname);
	
	snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s", uname, filename);
	snprintf(pathto, sizeof pathto, "/home/%s/shift2/drakor/%s/",uname,str);

	char *argv[] = {"mv", path, pathto, NULL};
	char *cmd = "/usr/bin/mv";
	bash(cmd, argv);

  	return;
}

void loadFile(){
		DIR *repo;
	   	struct dirent *dir;
	    snprintf(path, sizeof path, "/home/%s/shift2/drakor", uname);
	    repo = opendir(path);
		int count = 0;
		char cat[7][10]={"action","comedy","fantasy","horror","romance","school","thriller"};
	    if (repo)
	    {
	        while ((dir = readdir(repo)) != NULL)
	        {
	            if(dir->d_type==DT_REG){
	    			char *result;
	            	for ( int i = 0; i <7; i++){
	            		char genre[20];
	                	char *str= cat[i];
	            		snprintf(genre, sizeof genre, "%s.png", str);
	                	result = strstr(dir->d_name, genre);
	                	char *filename;
	                	filename = dir->d_name;
	                	if	( result ) {
	                		moveFile(filename,str);
						}
	            	}
	            }
	        }
	        closedir(repo);
	    }
	    mkdata();
}

void execute(){
	pid_t child;
	child = fork();
	int stat;

	if	(child < 0) exit(EXIT_FAILURE);
	
	if (child == 0) {
		unzip();
	} else {
		while(wait(&stat) > 0){
			loadFile();
		}
	}
	
}

int main() {

	uname=(char *)malloc(10*sizeof(char));
	uname=getlogin();

	pid_t child_id;
	child_id = fork();
	int status;

	if	(child_id < 0) exit(EXIT_FAILURE);
	
	if (child_id == 0) {
		mkrepo(uname);	
	} else {
		while(wait(&status) > 0){
			execute();
		}
	}
	
}
