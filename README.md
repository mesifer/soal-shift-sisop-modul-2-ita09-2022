# soal-shift-modul-2-ITA09-2022

| No | Nama | NRP |
| --- | --- | --- |
| 1 | Bagus Ridho R | 5027201043 |
| 2 | Fairuz Azhar A | 5027201059 |
| 3 | Rafael Nixon | 05311940000025 |

# Soal No 1
Masih Belum Menemukan solusi

# Soal No 2
( revisi )
## 1. Membuat repository di "/home/usr/shift2/drakor"
```c
    void mkrepo(char *uname){
        snprintf(path, sizeof path, "/home/%s/shift2/drakor", uname);
        char *argv[ ] = {"mkdir", "-p", path, NULL};	
        execv("/usr/bin/mkdir", argv);
    }
```
## 2. Unzip file drakor ke dalam repo "/home/usr/shift2/drakor" dengan ekstensi ".png"
```c
    void unzip(){
        char *argv[ ] = {"unzip", "drakor.zip", "*.png", "-d", "/home/rosfandy/shift2/drakor", NULL};
        execv("/bin/unzip", argv);
    }
```
## 3. Scan tiap nama file di repo tsb, dan pindahkan ke dalam folder kategori masing-masing genre drakor. 
### Penjelasan
Masing-masing file di repo "/home/usr/shift2/drakor", dan siapkan array berisi genre yang sudah ditentukan. Gunakan fungsi "strstr" untuk menscanning apakah terdapat "genre" di dalam namafile tersebut, jika ya maka buat repo sesuai "genre" dan pindahkan file tsb.
```c
    void loadFile(){
            DIR *repo;
            struct dirent *dir;
            snprintf(path, sizeof path, "/home/%s/shift2/drakor", uname);
            repo = opendir(path);
            int count = 0;
            char cat[7][10]={"action","comedy","fantasy","horror","romance","school","thriller"};
            if (repo)
            {
                while ((dir = readdir(repo)) != NULL)
                {
                    if(dir->d_type==DT_REG){
                        char *result;
                        for ( int i = 0; i <7; i++){
                            char genre[20];
                            char *str= cat[i];
                            snprintf(genre, sizeof genre, "%s.png", str);
                            result = strstr(dir->d_name, genre);
                            char *filename;
                            filename = dir->d_name;
                            if	( result ) {
                                moveFile(filename,str);
                            }
                        }
                    }
                }
                closedir(repo);
            }
            mkdata();
}
```
## 4. Dupilkasi file dengan ekstensi "_" kemudian pindahkan file duplikat tersebut ke dalam folder genre drakor yang terduplikasi. 
### Penjelasan
Masing-masing file di folder genre akan dicek, kemudian akan dilakukan penggunaan fungsi "strtok" untuk mengambil string di tiap notasi "_". Jika kondisi terpenuhi, maka copy file tsb ke dalam folder genre selanjutnya. 
```c
    void mkdata(){
        DIR *repo;
        struct dirent *dir;
        char genre[7][10]={"action","comedy","fantasy","horror","romance","school","thriller"};
        char data[5][50];
        char d_split[1][50];
        for (int i = 0; i < 7; i++){
        
        snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/", uname, genre[i]);
        repo = opendir(path);
        if (repo)
        {
            while ((dir = readdir(repo)) != NULL)
            {
                char *result;
                    char filename[1000];
                    snprintf(filename, sizeof filename, "%s", dir->d_name);
                    result = strstr(filename,"_");
                    if (result) {
                        char file[1000];
                        strcpy(file, filename);
                        
                        char *split = strtok(file,"_");
                        int j = 0;
                        while(split !=NULL){
                            snprintf(d_split[j], sizeof d_split[j], "%s", split);
                            j++;
                            split=strtok(NULL,"_");
                        }
                        int k = 0;
                            char judul[j][50];
                            char tahun[j][50];
                            char genre[j][50];
                            char kat[50];
                            for (int j=0; j<2; j++){
                                char *token = strtok(d_split[j],";");

                                snprintf(judul[j], sizeof judul[j], "%s", token);
                                token = strtok(NULL,";");
                                // printf("%s\n",judul[j]);

                                snprintf(tahun[j], sizeof tahun[j], "%s", token);
                                token = strtok(NULL,";");
                                // printf("%s\n",tahun[j]);
                                                    
                                snprintf(genre[j], sizeof genre[j], "%s", token);
                                // printf("%s\n",genre[j]);
                                token = strtok(NULL,";");
                                char *kategori=strtok(genre[1],".");
                                
                                int i = 0;
                                snprintf(kat, sizeof kat, "%s", kategori);
                                // printf("%s\n",kat);
                                i++;
                                kategori = strtok(NULL,".");

                            }
                                copy(genre[0],kat,filename);

                        
                    }
                    
            }
            closedir(repo);
        }
        }

        chname();	 
    }
```
## 5. Rename masing-masing file di repo dengan judul drakor file tsb, dan masukan data (judul, tahun, kategori) ke dalam file "data.txt"
### Penjelasan
Masing-masing file di folder genre akan dicek, cek terlebih dahulu file dengan nama tanpa notasi " _ ". Kemudian gunakan fungsi "strtok" untuk menyeleksi string di tiap notasi " ; " dan rename file tsb sesuai judul dan masukan (judul, tahun, kategori) ke dalam "data.txt". Lalu, file dengan notasi " _ " akan dipisah menjadi dua string, kemudian jika di repo "genre" terdapat file bernama "genre".png maka ambil string kedua dalam filename tsb dan rename sesuai judul dan masukan (judul,tahun,kategori) ke dalam "data.txt"
```c
void chname(){
	DIR *repo;
	struct dirent *dir;
	char genre[7][10]={"action","comedy","fantasy","horror","romance","school","thriller"};

	for (int i = 0; i<7; i++){
		snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/", uname, genre[i]);
		repo = opendir(path);
		if (repo){
			while ((dir = readdir(repo)) != NULL){
			if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) continue;
				// printf("%s\wn",path);
				char filename[1000];
				char file[1000];
				char *result;
				snprintf(filename, sizeof filename, "%s", dir->d_name);
				strcpy(file, filename);
				// printf("%s\n", filename);

				result = strstr(file,"_");
				
				if(!result){
					char judul[50];				
					char tahun[50];				
					char kategori[50];
					printf("%s\n", filename);
					char *token = strtok(file,";");
					snprintf(judul, sizeof judul, "%s", token);
					// printf("%s\n", judul);
					token = strtok(NULL, ";");
					
					snprintf(tahun, sizeof judul, "%s", token);
					// printf("%s\n",tahun);
					token = strtok(NULL, ";");
			
					snprintf(kategori, sizeof judul, "%s", token);
					char nama[100];
					snprintf(nama, sizeof judul, "%s", kategori);
					strtok(nama,".");
					// printf("%s\n",nama);
					token = strtok(NULL, ";");
					
					ganti(judul,genre[i],filename);
					data(judul,tahun,nama,genre[i]);
				} else {
					// printf("%s\n",filename);
					char str[1000];
					snprintf(str, sizeof str, "%s", filename);
					char *split = strtok(file,"_");
					while (split!=NULL){
						// printf("%s\n",split);
						char *flag;
						char ext[100];
						snprintf(ext, sizeof ext, "%s.png", genre[i]);
						flag = strstr(split, ext);
						if (flag){
							char judull[50];				
							char tahunn[50];				
							char kategorii[50];

							char *tokenn = strtok(split,";");
							snprintf(judull, sizeof judull, "%s", tokenn);
							tokenn = strtok(NULL, ";");

							snprintf(tahunn, sizeof tahunn, "%s", tokenn);
							tokenn = strtok(NULL, ";");
					
							snprintf(kategorii, sizeof kategorii, "%s", tokenn);
							tokenn = strtok(NULL, ";");

							ganti(judull,genre[i],filename);
							data(judull,tahunn,kategorii,genre[i]);
							
						} else {
							char judull[50];				
							char tahunn[50];				
							char kategorii[50];

							char *tokenn = strtok(split,";");
							snprintf(judull, sizeof judull, "%s", tokenn);
							tokenn = strtok(NULL, ";");

							snprintf(tahunn, sizeof tahunn, "%s", tokenn);
							tokenn = strtok(NULL, ";");
					
							snprintf(kategorii, sizeof kategorii, "%s", tokenn);
							tokenn = strtok(NULL, ";");							

							ganti(judull,genre[i],filename);
							data(judull,tahunn,kategorii,genre[i]);
						}
						split=strtok(NULL,"_");
					}			
				}

			}
		
		}
	}
	
}
```
## 6. Untuk fungsi "ganti" = rename file, "data" = buat data.txt, dan "copy" = duplikat file dengan ekst "_"
```c
    void ganti(char judul[50], char genre[200], char filename[1000]){
        char pathto[200];
        snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/%s", uname, genre, filename);
        snprintf(pathto, sizeof pathto, "/home/%s/shift2/drakor/%s/%s.png", uname, genre, judul);
        char *argv[] = {"mv", path, pathto, NULL};
        char *cmd = {"/usr/bin/mv"};
        bash(cmd,argv);
    }

    void data(char judul[100], char tahun[100], char kategori[100], char genre[100]){
        snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/data.txt", uname, genre);
        FILE *data;
        data = fopen(path, "a");

        

        if (strcmp(temp, kategori) != 0){
            fprintf(data, "kategori : %s\n\n", kategori);
        }
        fprintf(data, "nama : %s\n", judul);
        fprintf(data, "rilis : tahun %s\n\n", tahun);
        
        fclose(data);
        strcpy(temp,kategori);
    }

    void copy(char genrefrom[50], char genreto[50], char filename[1000]){
            char pathto[200];
            snprintf(path, sizeof path, "/home/%s/shift2/drakor/%s/%s", uname, genreto, filename);
            snprintf(pathto, sizeof pathto, "/home/%s/shift2/drakor/%s/", uname, genrefrom);
            char *argv[] = {"cp", path, pathto, NULL};
            char *cmd = "/usr/bin/cp";
            bash(cmd, argv);
    }    
```
### hasil :
##### direktory genre di repo '/home/user/shift2/drakor/
![image](img/dir.png)

#### isi file masing-masing dir :
##### comedy

![image](img/soal2/comedy.png)




# Soal No 3
(revisi)
## 1. Mendeklarasikan lname untuk nama dan struct dari dirent
```c
    char *lname;
    struct dirent *dStru;
```

## 2. Mendeklarasikan fungsi forking, forkMove, dan forkRemove
### Penjelasan
fungsi forking digunakan untuk menjalankan perintah bash
fungsi forkMove digunakan untuk menjalankan perintah mv pada bash (melakukan move file atau memindahkan file)
fungsi forkRemove digunakan untuk menjalankan perintah rm pada bash (melakukan remove file atau menghapus file)
dimana ketiganya dilakukan dengan cara mendeklarasikan pid sebagai pid_t dan mengisinya dengan hasil dari perintah fork(). Bila nilai dari pid tersebut adalah nol(0), maka dia adalah child dan akan melakukan eksekusi perintah bash  
```c
    void forking(char cmd[], char *arg[]) {
        pid_t pid = fork();
        int s;
        if(pid == 0){
            execv(cmd, arg);
        }
        else {
            ((wait(&s)) > 0);
        }
    }

    void forkMove(char fileName[], char pathDestination[]) {
        pid_t pid = fork();
        int s;
        if(pid == 0) {
            execlp("mv", "mv", fileName, pathDestination, NULL);
        }
        else {
            ((wait(&s)) > 0);
        }
    }

    void forkRemove(char fileName[]) {
        pid_t pid = fork();
        int s;
        if(pid == 0) {
            execlp("rm", "rm", fileName, NULL);
        }
        else {
            ((wait(&s)) > 0);
        }
    }
```

## 3. Mendeklarasikan fungsi CreateDirAndUnzip
### Penjelasan
fungsi ini digunakan untuk melakukan pembuatan directory yang dibutuhkan dan untuk melakukan Unzip terhadap file animal.zip
```c
    void CreateDirAndUnzip() {
        char *modulArg[] = {"mkdir", "modul2", NULL};
        forking("/bin/mkdir", modulArg);

        char *animalArg[] = {"mkdir", "modul2/animal", NULL};
        forking("/bin/mkdir", animalArg);

        char *daratArg[] = {"mkdir", "modul2/darat", NULL};
        forking("/bin/mkdir", daratArg);

        sleep(3);
        
        char *airArg[] = {"mkdir", "modul2/air", NULL};
        forking("/bin/mkdir", airArg);

        char *unzipArg[] = {"unzip", "animal.zip", "-d", "./modul2", NULL};
        forking("/usr/bin/unzip", unzipArg);
    }
```

## 4. Mendeklarasikan fungsi Checker, CheckerDarat, CheckerAir
### Penjelasan
fungsi ini digunakan untuk melakukan pemisahan gambar-gambar dari file animal.zip yang telah di ekstrak tadi dan juga untuk membuat file list.txt dengan menggunakan fungsi-fungsi forking, forkMove, dan forkRemove
```c
    void Checker() {

        DIR *unzLoc;

        unzLoc = opendir("./modul2/animal");

        if(unzLoc != NULL) {
            while((dStru = readdir(unzLoc))) {
                if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0 && strstr(dStru->d_name, "darat")) {
                    char started[128] = "modul2/animal/";
                    strcat(started, dStru->d_name); 
                    forkMove(started, "modul2/darat");
                } else if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0 && strstr(dStru->d_name, "air")) {
                    char started[128] = "modul2/animal/";
                    strcat(started, dStru->d_name); 
                    forkMove(started, "modul2/air");
                } else {
                    if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0) {
                        char started[128] = "modul2/animal/";
                        strcat(started, dStru->d_name); 
                        forkRemove(started);
                    }
                    
                }
            }
            (void) closedir (unzLoc);
        } else  {
            perror ("Check again the directory or path!");
        }
    }

    void CheckerDarat() {
        DIR *daratP;

        daratP = opendir("modul2/darat");
        if(daratP != NULL) {
            while((dStru = readdir(daratP))) {
                if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0 && strstr(dStru->d_name, "bird")) {
                    char started[128] = "modul2/darat/";
                    strcat(started, dStru->d_name); 
                    forkRemove(started);
                }
            }
            (void) closedir (daratP);
        } else  {
            perror ("Check again the directory or path!");
        }
    }

    void CheckerAir() {
        DIR *airP;

        airP = opendir("modul2/air");
        FILE *listAir = fopen("./modul2/air/list.txt", "w");

        struct stat stats;
        int i;

        if(airP != NULL) {
            while((dStru = readdir(airP))) {
                if (strcmp(dStru->d_name, ".") != 0 && strcmp(dStru->d_name, "..") != 0 && strstr(dStru->d_name, "jpg")) {
                    char started[128] = "modul2/air/";
                    strcat(started, dStru->d_name);
                    i = stat(started, &stats);
                    char r = '_', w = '_', x = '_';
                    if(stats.st_mode & S_IRUSR) r = 'r';
                    if(stats.st_mode & S_IWUSR) w = 'w';
                    if(stats.st_mode & S_IXUSR) x = 'x';
                    fprintf(listAir, "%s_%c%c%c_%s\n", lname, r, w, x, dStru->d_name);
                }
            }
            (void) closedir (airP);
        } else  {
            perror ("Check again the directory or path!");
        }

        fclose(listAir);
    }
```


## 5. Memanggil fungsi-fungsi pada int main
```c
    int main() {

        lname = (char *)malloc(10*sizeof(char));
        lname = getlogin();

        CreateDirAndUnzip();
        Checker();
        CheckerDarat();

        return 0;
    }
```

## Pengecekan Direktori Sebelum dan Sesudah Menjalankan Program Soal 3
##### Ketika Belum Menjalankan Program
![image](img_soal3/3_0.jpg)
##### Setelah Melakukan Compile Dengan GCC
![image](img_soal3/3_1.jpg)
##### Ketika Program Sudah Dijalankan
![image](img_soal3/3_2.jpg)
##### Ketika Memasuki Direktori Modul2
![image](img_soal3/3_3.jpg)
##### Isi Dari Sub-Direktori Di Dalam Modul2
![image](img_soal3/3_4.jpg)
